package com.vivek;

import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AddOperationTest {
	private static AddOperation ap;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		ap = new AddOperation();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ap = null;
	}

	@BeforeEach
	void setUp() throws Exception {
		System.out.println("   Before target method   ");
	}

	@AfterEach
	void tearDown() throws Exception {
		System.out.println("   after target method   ");

	}

	@Test
	void testCheckNumberPositive() {
		Assert.assertEquals(9, ap.addTwoNumber(5, 4));

	}

	@Test
	void testCheckWithBothNegativeNumber() {
		Assert.assertEquals(-9, ap.addTwoNumber(-5, -4));

	}

	@Test
	void testCheckWithZero() {
		Assert.assertEquals(0, ap.addTwoNumber(0, 0));

	}

	@Test
	void testCheckpositiveAndNegative() {
		Assert.assertEquals(-4, ap.addTwoNumber(5, -9));

	}

	@Test
	void testCheckNegativeAndpositive() {
		Assert.assertEquals(4, ap.addTwoNumber(-5, 9));

	}

}
